package pl.mardom92.springsecurityjwt.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/admin-controller")
public class AdminController {

    @GetMapping
    public ResponseEntity<String> sayAdminHello() {
        return ResponseEntity.ok("Hello from secured admin endpoint");
    }
}
