package pl.mardom92.springsecurityjwt.common.response;

public class ResponseMessages {

    public static final String INVALID_REVOKED_MISSING_TOKEN = "Invalid, revoked or missing token";
    public static final String LOGGED_OUT_SUCCESSFULLY = "Logged out successfully";
    public static final String CANNOT_REFRESH_TOKEN = "Cannot refresh token";

}
