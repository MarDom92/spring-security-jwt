package pl.mardom92.springsecurityjwt.common.response;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Map;

public class ResponseUtils {

    public static void sendResponse(HttpServletResponse response, int status, ResponseType type, String message) {
        response.setStatus(status);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(response.getWriter(), Map.of(type.getValue(), message));
        } catch (IOException e) {
        }
    }
}
