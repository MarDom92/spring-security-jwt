package pl.mardom92.springsecurityjwt.common.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ResponseType {
    MESSAGE("message"),
    ERROR("error");

    private final String value;
}
