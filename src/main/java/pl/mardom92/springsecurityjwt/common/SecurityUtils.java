package pl.mardom92.springsecurityjwt.common;

import jakarta.servlet.http.HttpServletRequest;

import java.util.Objects;

public class SecurityUtils {

    public static String getAuthorizationHeader(HttpServletRequest request) {
        return request.getHeader("Authorization");
    }

    public static boolean validateBearerTokenHeader(String authHeader) {
        if (Objects.isNull(authHeader) || !authHeader.startsWith("Bearer ")) {
            return false;
        }
        return true;
    }

    public static String extractAuthTokenFromHeader(String authHeader, int index) {
        return authHeader.substring(index);
    }
}
