package pl.mardom92.springsecurityjwt.auth.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.mardom92.springsecurityjwt.auth.model.AuthenticationRequest;
import pl.mardom92.springsecurityjwt.auth.model.AuthenticationResponse;
import pl.mardom92.springsecurityjwt.auth.model.RegisterRequest;
import pl.mardom92.springsecurityjwt.common.SecurityUtils;
import pl.mardom92.springsecurityjwt.common.response.ResponseMessages;
import pl.mardom92.springsecurityjwt.common.response.ResponseType;
import pl.mardom92.springsecurityjwt.common.response.ResponseUtils;
import pl.mardom92.springsecurityjwt.config.JwtService;
import pl.mardom92.springsecurityjwt.token.model.Token;
import pl.mardom92.springsecurityjwt.token.model.TokenType;
import pl.mardom92.springsecurityjwt.token.repository.TokenRepository;
import pl.mardom92.springsecurityjwt.user.model.Role;
import pl.mardom92.springsecurityjwt.user.model.User;
import pl.mardom92.springsecurityjwt.user.repository.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(RegisterRequest request) {
        User user = User.builder()
                .firstname(request.getFirstname())
                .lastname(request.getLastname())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.USER)
                .build();

        User savedUser = userRepository.save(user);
        String jwtToken = jwtService.generateJwtToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        saveUserToken(savedUser, jwtToken);

        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    public AuthenticationResponse authenticate(AuthenticationRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );

        User user = userRepository.findByEmail(request.getEmail()).orElseThrow();
        String jwtToken = jwtService.generateJwtToken(user);
        String refreshToken = jwtService.generateRefreshToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, jwtToken);

        return AuthenticationResponse.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .build();
    }

    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;

        boolean isValidatedToken = SecurityUtils.validateBearerTokenHeader(authHeader);

        if (!isValidatedToken) {
            ResponseUtils.sendResponse(
                    response,
                    HttpServletResponse.SC_OK,
                    ResponseType.ERROR,
                    ResponseMessages.CANNOT_REFRESH_TOKEN
            );
            return;
        }

        refreshToken = SecurityUtils.extractAuthTokenFromHeader(authHeader, 7);
        userEmail = jwtService.extractUsername(refreshToken);

        if (Objects.nonNull(userEmail)) {
            User user = this.userRepository.findByEmail(userEmail)
                    .orElseThrow();

            if (jwtService.isTokenValid(refreshToken, user)) {
                String accessToken = jwtService.generateJwtToken(user);
                revokeAllUserTokens(user);
                saveUserToken(user, accessToken);
                AuthenticationResponse authResponse = AuthenticationResponse.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }
        }
    }

    private void saveUserToken(User user, String jwtToken) {
        Token token = Token.builder()
                .user(user)
                .value(jwtToken)
                .tokenType(TokenType.BEARER)
                .expired(false)
                .revoked(false)
                .build();
        tokenRepository.save(token);
    }

    private void revokeAllUserTokens(User user) {
        List<Token> validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty()) {
            return;
        }
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }
}
