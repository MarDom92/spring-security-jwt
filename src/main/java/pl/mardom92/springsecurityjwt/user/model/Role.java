package pl.mardom92.springsecurityjwt.user.model;

public enum Role {

    USER,
    ADMIN
}
