package pl.mardom92.springsecurityjwt.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;
import pl.mardom92.springsecurityjwt.common.SecurityUtils;
import pl.mardom92.springsecurityjwt.common.response.ResponseMessages;
import pl.mardom92.springsecurityjwt.common.response.ResponseType;
import pl.mardom92.springsecurityjwt.common.response.ResponseUtils;
import pl.mardom92.springsecurityjwt.token.model.Token;
import pl.mardom92.springsecurityjwt.token.repository.TokenRepository;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {

    private final TokenRepository tokenRepository;

    @Override
    public void logout(HttpServletRequest request,
                       HttpServletResponse response,
                       Authentication authentication) {

        final String authHeader = SecurityUtils.getAuthorizationHeader(request);
        final String jwtToken;

        boolean isValidatedToken = SecurityUtils.validateBearerTokenHeader(authHeader);

        if (!isValidatedToken) {
            ResponseUtils.sendResponse(
                    response,
                    HttpServletResponse.SC_BAD_REQUEST,
                    ResponseType.MESSAGE,
                    ResponseMessages.INVALID_REVOKED_MISSING_TOKEN
            );
            return;
        }

        jwtToken = SecurityUtils.extractAuthTokenFromHeader(authHeader, 7);
        Token storedToken = tokenRepository.findByValue(jwtToken)
                .orElse(null);

        if (Objects.isNull(storedToken)
                || storedToken.isRevoked()) {
            ResponseUtils.sendResponse(
                    response,
                    HttpServletResponse.SC_BAD_REQUEST,
                    ResponseType.ERROR,
                    ResponseMessages.INVALID_REVOKED_MISSING_TOKEN
            );
            return;
        }

        if (Objects.nonNull(storedToken)) {
            storedToken.setExpired(true);
            storedToken.setRevoked(true);
            tokenRepository.save(storedToken);
            SecurityContextHolder.clearContext();

            ResponseUtils.sendResponse(
                    response,
                    HttpServletResponse.SC_OK,
                    ResponseType.MESSAGE,
                    ResponseMessages.LOGGED_OUT_SUCCESSFULLY
            );
        }
    }
}
