package pl.mardom92.springsecurityjwt.token.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mardom92.springsecurityjwt.token.repository.TokenRepository;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final TokenRepository tokenRepository;

    public boolean isTokenValid(String jwtToken) {
        return tokenRepository.findByValue(jwtToken)
                .map(t ->
                        !t.isExpired() && !t.isRevoked())
                .orElse(false);
    }
}
